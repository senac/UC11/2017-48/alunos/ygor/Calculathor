package br.com.senac.calculathor;

import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.Double;

public class MainActivity extends AppCompatActivity {
    private Double num1 = 0.0;
    private Double num2 = 0.0;
    private String sinal;

    private void init(){
        int orientacao = getResources().getConfiguration().orientation;
        if(orientacao == Configuration.ORIENTATION_PORTRAIT){
            Toast.makeText(this,"Orientção Portrait",Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(this,"Orientação Landscape", Toast.LENGTH_LONG).show();
        }

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        final TextView resultado = (TextView) findViewById(R.id.resultado);
       final Button  btn1  = (Button)findViewById(R.id.um) ;
        final Button  btn2 = (Button) findViewById(R.id.dois);
         final Button  btn3 = (Button) findViewById(R.id.tres);
         final Button  btn4 = (Button) findViewById(R.id.quatro);
         final Button  btn5 = (Button) findViewById(R.id.cinco);
         final Button  btn6 = (Button) findViewById(R.id.seis);
        final       Button  btn7 = (Button) findViewById(R.id.sete);
        final Button  btn8 =  (Button) findViewById(R.id.oito);
        final Button  btn9 = (Button) findViewById(R.id.nove);
        final  Button  btn0 = (Button) findViewById(R.id.zero);
        final Button  btnigual =(Button)findViewById(R.id.igual) ;
        final  Button  btnsoma = (Button) findViewById(R.id.soma);
        final  Button  btndiv =  (Button) findViewById(R.id.div);
        final  Button  btnmult = (Button) findViewById(R.id.mult);
        final Button  btnsub = (Button) findViewById(R.id.sub);
        final Button  btnponto = (Button) findViewById(R.id.ponto);
        final Button  btnlimpar = (Button) findViewById(R.id.btnCE);
        final Button  btnporcento =  (Button) findViewById(R.id.porcento);
        final Button btnsqrt = (Button) findViewById(R.id.sqrt);


      btn1.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
              if(resultado.getText().toString().equals("0")){
                  resultado.setText(" ");
                  resultado.setText(resultado.getText().toString()+"1");
          }else{
                  resultado.setText(resultado.getText().toString() + "1");}}
      });


      btn2.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {

              if(resultado.getText().toString().equals("0")){
                  resultado.setText(" ");
                  resultado.setText(resultado.getText().toString()+"2");
              }else{
                  resultado.setText(resultado.getText().toString() + "2");}}


      });


      btn3.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {

              if(resultado.getText().toString().equals("0")){
                  resultado.setText(" ");
                  resultado.setText(resultado.getText().toString()+"3");
              }else{
                  resultado.setText(resultado.getText().toString() + "3");}}


      });

      btn4.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {


              if(resultado.getText().toString().equals("0")){
                  resultado.setText(" ");
                  resultado.setText(resultado.getText().toString()+"4");
              }else {
                  resultado.setText(resultado.getText().toString() + "4");}}
      });


      btn5.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
              if(resultado.getText().toString().equals("0")){
                  resultado.setText(" ");
                  resultado.setText(resultado.getText().toString()+"5");
              }else{
                  resultado.setText(resultado.getText().toString() + "5");}
          }
      });


      btn6.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
              if(resultado.getText().toString().equals("0")){
                  resultado.setText(" ");
                  resultado.setText(resultado.getText().toString()+"6");
              }else{
                  resultado.setText(resultado.getText().toString() + "6");}
          }
      });

      btn7.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {

              if(resultado.getText().toString().equals("0")){
                  resultado.setText(" ");
                  resultado.setText(resultado.getText().toString()+"7");
              }else{
                  resultado.setText(resultado.getText().toString() + "7");}
          }
      });
      btn8.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
              if(resultado.getText().toString().equals("0")){
                  resultado.setText(" ");
                  resultado.setText(resultado.getText().toString()+"8");
              }else{
                  resultado.setText(resultado.getText().toString() + "8");}
          }
      });
      btn9.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
              if(resultado.getText().toString().equals("0")){
                  resultado.setText(" ");
                  resultado.setText(resultado.getText().toString()+"9");
              }else{
                  resultado.setText(resultado.getText().toString() + "9");}
          }
      });


      btn0.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
              if(resultado.getText().toString().equals("0")){
                  resultado.setText(" ");
                  resultado.setText(resultado.getText().toString()+"0");
              }else{
                  resultado.setText(resultado.getText().toString() + "0");}
          }
      });

      btnsoma.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
              sinal = "+";
             num1 = Double.valueOf(resultado.getText().toString());
              //resultado.setText(" ");
              resultado.setText("");


          }
      });


      btnsub.setOnClickListener(new View.OnClickListener() {


          @Override
          public void onClick(View view) {
              sinal = "-";
              num1 = Double.valueOf(resultado.getText().toString());
              resultado.setText("");

          }
      });

      btndiv.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
              sinal = "/";
             num1 = Double.valueOf(resultado.getText().toString());
              resultado.setText("");


          }
      });

      btnmult.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
              sinal = "*";
              num1 = Double.valueOf(resultado.getText().toString());
              resultado.setText(" ");

          }
      });

      btnigual.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {

              num2 = Double.valueOf(resultado.getText().toString());
              resultado.setText(calculadora());
          }
      });

      btnlimpar.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
              resultado.setText("0");
          }
      });

      btnponto.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
              resultado.setText(resultado.getText().toString()+".");
          }
      });


      btnporcento.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
              sinal = "&";
              num1 = Double.valueOf(resultado.getText().toString());
          }
      });
      btnsqrt.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
              sinal ="2^";
              num1 = Double.valueOf(resultado.getText().toString());


          }
      });

}


private String calculadora(){
        Double resultado = 0.0;
  switch(sinal) {
      case "+":
          resultado = num1+num2;
          break;
      case "-":
          resultado = num1-num2;

       break;
      case "/":
          resultado = num1/num2;
       break;
      case "*":
          resultado = num1*num2;
       break;
      case "&":
          resultado = (num1*num2)/100;
          break;
      case "2^":
          sqrt();


  }
    return String.valueOf(resultado);

}
 public Double sqrt(){

    return Math.sqrt(num1);
 }


}


